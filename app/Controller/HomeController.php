<?php
    
    class HomeController extends AppController
    {
        public $uses = ['Story'];
        
        public function index()
        {
            $stories = $this->Story->find('all', ['conditions' => ['is_public' => true]]);
            $this->set(compact('stories'));
        }
    }