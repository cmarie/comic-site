<?php
    App::uses('AppController', 'Controller');
    
    /**
     * Clips Controller
     *
     * @property Clip               $Clip
     * @property PaginatorComponent $Paginator
     */
    class ClipsController extends AppController
    {
        /**
         * Components
         *
         * @var array
         */
        public $components = ['Paginator'];
        
        /**
         * index method
         *
         * @return void
         */
        public function index()
        {
            $this->Clip->recursive = 2;
            $this->set('clips', $this->Paginator->paginate());
        }
        
        /**
         * add method
         *
         * @return void
         */
        public function add()
        {
            if($this->request->is('post')) {
                $this->Clip->create();
                if($this->Clip->save($this->request->data)) {
                    $this->Flash->success(__('The clip has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The clip could not be saved. Please, try again.'));
                }
            }
            $items = $this->Clip->Scene->find('all');
            $scenes = [];
            foreach($items as $item) {
                $scenes[$item['Scene']['id']] = $item['Story']['name'] . ' - ' . $item['Scene']['name'];
            }
            $this->set(compact('scenes'));
        }
        
        /**
         * edit method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function edit($id = null)
        {
            if(!$this->Clip->exists($id)) {
                throw new NotFoundException(__('Invalid clip'));
            }
            if($this->request->is(['post', 'put'])) {
                if($this->Clip->save($this->request->data)) {
                    $this->Flash->success(__('The clip has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The clip could not be saved. Please, try again.'));
                }
            } else {
                $options = ['conditions' => ['Clip.' . $this->Clip->primaryKey => $id]];
                $this->request->data = $this->Clip->find('first', $options);
            }
            $items = $this->Clip->Scene->find('all');
            $scenes = [];
            foreach($items as $item) {
                $scenes[$item['Scene']['id']] = $item['Story']['name'] . ' - ' . $item['Scene']['name'];
            }
            $this->set(compact('scenes'));
        }
        
        /**
         * delete method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function delete($id = null)
        {
            $this->Clip->id = $id;
            if(!$this->Clip->exists()) {
                throw new NotFoundException(__('Invalid clip'));
            }
            $this->request->allowMethod('post', 'delete');
            if($this->Clip->delete()) {
                $this->Flash->success(__('The clip has been deleted.'));
            } else {
                $this->Flash->error(__('The clip could not be deleted. Please, try again.'));
            }
            
            return $this->redirect(['action' => 'index']);
        }
    }
