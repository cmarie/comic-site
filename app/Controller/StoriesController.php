<?php
    App::uses('AppController', 'Controller');
    
    /**
     * Stories Controller
     *
     * @property Story              $Story
     * @property PaginatorComponent $Paginator
     */
    class StoriesController extends AppController
    {
        /**
         * Components
         *
         * @var array
         */
        public $components = ['Paginator'];
        
        public function beforeFilter()
        {
            parent::beforeFilter();
            // Allow users to register and logout.
            $this->Auth->allow('view');
        }
        
        /**
         * index method
         *
         * @return void
         */
        public function index()
        {
            $this->Story->recursive = 0;
            $this->set('stories', $this->Paginator->paginate());
        }
        
        /**
         * view method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function view($id = null)
        {
            if(!$this->Story->exists($id)) {
                throw new NotFoundException(__('Invalid story'));
            }
            $this->Story->recursive = 2;
            $story = $this->Story->read(null, $id);
            if(!$story['Story']['is_public']) {
                throw new NotFoundException(__('Invalid story'));
            }
            $this->set(compact('story'));
        }
        
        /**
         * add method
         *
         * @return void
         */
        public function add()
        {
            if($this->request->is('post')) {
                $this->Story->create();
                if($this->Story->save($this->request->data)) {
                    $this->Flash->success(__('The story has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The story could not be saved. Please, try again.'));
                }
            }
        }
        
        /**
         * edit method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function edit($id = null)
        {
            if(!$this->Story->exists($id)) {
                throw new NotFoundException(__('Invalid story'));
            }
            if($this->request->is(['post', 'put'])) {
                if($this->Story->save($this->request->data)) {
                    $this->Flash->success(__('The story has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The story could not be saved. Please, try again.'));
                }
            } else {
                $options = ['conditions' => ['Story.' . $this->Story->primaryKey => $id]];
                $this->request->data = $this->Story->find('first', $options);
            }
        }
        
        /**
         * delete method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function delete($id = null)
        {
            $this->Story->id = $id;
            if(!$this->Story->exists()) {
                throw new NotFoundException(__('Invalid story'));
            }
            $this->request->allowMethod('post', 'delete');
            if($this->Story->delete()) {
                $this->Flash->success(__('The story has been deleted.'));
            } else {
                $this->Flash->error(__('The story could not be deleted. Please, try again.'));
            }
            
            return $this->redirect(['action' => 'index']);
        }
    }
