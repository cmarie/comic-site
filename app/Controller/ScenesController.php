<?php
    App::uses('AppController', 'Controller');
    
    /**
     * Scenes Controller
     *
     * @property Scene              $Scene
     * @property PaginatorComponent $Paginator
     */
    class ScenesController extends AppController
    {
        /**
         * Components
         *
         * @var array
         */
        public $components = ['Paginator'];
    
        public function beforeFilter()
        {
            parent::beforeFilter();
            // Allow users to register and logout.
            $this->Auth->allow('view');
        }
        /**
         * index method
         *
         * @return void
         */
        public function index()
        {
            $this->Scene->recursive = 0;
            $this->set('scenes', $this->Paginator->paginate());
        }
        
        /**
         * view method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function view($id = null)
        {
            if(!$this->Scene->exists($id)) {
                throw new NotFoundException(__('Invalid scene'));
            }
            $options = ['conditions' => ['Scene.' . $this->Scene->primaryKey => $id]];
            $this->set('scene', $this->Scene->find('first', $options));
        }
        
        /**
         * add method
         *
         * @return void
         */
        public function add()
        {
            if($this->request->is('post')) {
                $this->Scene->create();
                if($this->Scene->save($this->request->data)) {
                    $this->Flash->success(__('The scene has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The scene could not be saved. Please, try again.'));
                }
            }
            $stories = $this->Scene->Story->find('list');
            $this->set(compact('stories'));
        }
        
        /**
         * edit method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function edit($id = null)
        {
            if(!$this->Scene->exists($id)) {
                throw new NotFoundException(__('Invalid scene'));
            }
            if($this->request->is(['post', 'put'])) {
                if($this->Scene->save($this->request->data)) {
                    $this->Flash->success(__('The scene has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The scene could not be saved. Please, try again.'));
                }
            } else {
                $options = ['conditions' => ['Scene.' . $this->Scene->primaryKey => $id]];
                $this->request->data = $this->Scene->find('first', $options);
            }
            $stories = $this->Scene->Story->find('list');
            $this->set(compact('stories'));
        }
        
        /**
         * delete method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function delete($id = null)
        {
            $this->Scene->id = $id;
            if(!$this->Scene->exists()) {
                throw new NotFoundException(__('Invalid scene'));
            }
            $this->request->allowMethod('post', 'delete');
            if($this->Scene->delete()) {
                $this->Flash->success(__('The scene has been deleted.'));
            } else {
                $this->Flash->error(__('The scene could not be deleted. Please, try again.'));
            }
            
            return $this->redirect(['action' => 'index']);
        }
    }
