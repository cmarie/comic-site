<ul class="pagination <?=$class;?>">
    <?
        echo $this->Paginator->prev(
            '<i class="fa fa-angle-left"></i>',
            array(
                'tag' => 'li',
                'escape' => false
            ),
            null,
            array(
                'disabledTag' => 'a',
                'class' => 'disabled'
            )
        );
        echo $this->Paginator->numbers(array(
            'separator' => null,
            'tag' => 'li',
            'currentTag' => 'a',
            'currentClass' => 'active'
        ));
        echo $this->Paginator->next(
            '<i class="fa fa-angle-right"></i>',
            array(
                'tag' => 'li',
                'escape' => false
            ),
            null,
            array(
                'disabledTag' => 'a',
                'class' => 'disabled',
                'escape' => false
            )
        );
    ?>
</ul>