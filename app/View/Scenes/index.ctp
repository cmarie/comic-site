<div class="scenes index">
    <h2><?php echo __('Scenes'); ?></h2>
    <table class="table">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('story_id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('is_public'); ?></th>
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($scenes as $scene): ?>
                <tr>
                    <td><?php echo h($scene['Scene']['id']); ?>&nbsp;</td>
                    <td>
                        <?php echo $this->Html->link($scene['Story']['name'], [
                            'controller' => 'stories',
                            'action' => 'view',
                            $scene['Story']['id']
                        ]); ?>
                    </td>
                    <td><?php echo h($scene['Scene']['name']); ?>&nbsp;</td>
                    <td><?php echo h($scene['Scene']['is_public'] ? 'Yes' : 'No'); ?>&nbsp;</td>
                    <td><?php echo h($scene['Scene']['created']); ?>&nbsp;</td>
                    <td><?php echo h($scene['Scene']['modified']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), [
                            'action' => 'view',
                            $scene['Scene']['id']
                        ], ['class' => 'btn btn-xs btn-success']); ?>
                        
                        <?php echo $this->Html->link(__('Edit'), [
                            'action' => 'edit',
                            $scene['Scene']['id']
                        ], ['class' => 'btn btn-xs btn-success']); ?>
                        
                        <?php echo $this->Form->postLink(__('Delete'), [
                            'action' => 'delete',
                            $scene['Scene']['id']
                        ], [
                            'class' => 'btn btn-xs btn-success',
                            'confirm' => __('Are you sure you want to delete # %s?', $scene['Scene']['id'])
                        ]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
            echo $this->Paginator->counter([
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ]);
        ?>    </p>
    <div class="paging">
        <?php
            echo $this->element('pagination', ['class' => 'primary']);
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">
        <li><?php echo $this->Html->link(__('New Scene'), ['action' => 'add']); ?></li>
        <li><?php echo $this->Html->link(__('List Stories'), ['controller' => 'stories', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Story'), ['controller' => 'stories', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Clips'), ['controller' => 'clips', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Clip'), ['controller' => 'clips', 'action' => 'add']); ?> </li>
    </ul>
</div>
