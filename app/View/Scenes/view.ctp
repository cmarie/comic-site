<h1><?php echo $scene['Story']['name']; ?> - <?php echo $scene['Scene']['name']; ?></h1>
<?php foreach($scene['Clip'] as $clip) { ?>
    <div class="col-xs-6 col-md-3 story">
        <a class="" href="/img/clips/<?php echo $clip['id']; ?>/<?php echo sha1(Configure::read('Security.salt') . $clip['id']); ?>_f.jpg"  data-toggle="lightbox" data-gallery="<?php echo $scene['Story']['name']; ?>">
            <img class="img-thumbnail" src="/img/clips/<?php echo $clip['id']; ?>/<?php echo sha1(Configure::read('Security.salt') . $clip['id']); ?>_p.jpg"/>
        </a>
    </div>
<?php } ?>
