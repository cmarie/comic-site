<div class="scenes form">
    <?php echo $this->BootstrapForm->create('Scene'); ?>
    <fieldset>
        <legend><?php echo __('Add Scene'); ?></legend>
        <?php
            echo $this->BootstrapForm->input('story_id');
            echo $this->BootstrapForm->input('name');
            echo $this->BootstrapForm->input('is_public');
        ?>
    </fieldset>
    <?php echo $this->BootstrapForm->submit(__('Create'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->BootstrapForm->end(); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">

        <li><?php echo $this->Html->link(__('List Scenes'), ['action' => 'index']); ?></li>
        <li><?php echo $this->Html->link(__('List Stories'), ['controller' => 'stories', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Story'), ['controller' => 'stories', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Clips'), ['controller' => 'clips', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Clip'), ['controller' => 'clips', 'action' => 'add']); ?> </li>
    </ul>
</div>
