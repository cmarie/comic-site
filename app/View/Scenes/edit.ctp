<div class="scenes form">
<?php echo $this->BootstrapForm->create('Scene'); ?>
	<fieldset>
		<legend><?php echo __('Edit Scene'); ?></legend>
	<?php
		echo $this->BootstrapForm->input('id');
		echo $this->BootstrapForm->input('story_id');
		echo $this->BootstrapForm->input('name');
		echo $this->BootstrapForm->input('is_public');
	?>
	</fieldset>
    <?php echo $this->BootstrapForm->submit(__('Save'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->BootstrapForm->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Scene.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Scene.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Scenes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stories'), array('controller' => 'stories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Story'), array('controller' => 'stories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clips'), array('controller' => 'clips', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Clip'), array('controller' => 'clips', 'action' => 'add')); ?> </li>
	</ul>
</div>
