<div class="users form">
    <?php echo $this->Form->create('User', ['class' => 'form-horizontal']); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <div class="form-group">
            <label for="UserUsername" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
                <input name="data[User][username]" maxlength="45" type="text" id="UserUsername" class="form-control">
            </div>

        </div>
        <div class="form-group">
            <label for="UserPassword" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input name="data[User][password]" type="password" id="UserPassword" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Register</button>
            </div>
        </div>
    </fieldset>
</div>