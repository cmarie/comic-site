<div class="clips form">
<?php echo $this->BootstrapForm->create('Clip', ['type' => 'file']); ?>
	<fieldset>
		<legend><?php echo __('Edit Clip'); ?></legend>
	<?php
		echo $this->BootstrapForm->input('id');
		echo $this->BootstrapForm->input('scene_id');
        echo $this->BootstrapForm->input('file', ['label' => 'Cover Image', 'type' => 'file']);
		echo $this->BootstrapForm->input('is_public');
	?>
	</fieldset>
    <?php echo $this->BootstrapForm->submit(__('Save'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->BootstrapForm->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Clip.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Clip.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Clips'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Scenes'), array('controller' => 'scenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scene'), array('controller' => 'scenes', 'action' => 'add')); ?> </li>
	</ul>
</div>
