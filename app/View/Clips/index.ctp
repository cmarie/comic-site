<div class="clips index">
    <h2><?php echo __('Clips'); ?></h2>
    <table class="table">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('scene_id'); ?></th>
                <th><?php echo $this->Paginator->sort('is_public'); ?></th>
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($clips as $clip): ?>
                <tr>
                    <td><?php echo h($clip['Clip']['id']); ?>&nbsp;</td>
                    <td>
                        <img class="img-thumbnail" src="/img/clips/<?php echo $clip['Clip']['id']; ?>/<?php echo sha1(Configure::read('Security.salt') . $clip['Clip']['id']); ?>_s.jpg">
                        <?php echo $this->Html->link($clip['Scene']['Story']['name'] . ' - ' . $clip['Scene']['name'], [
                            'controller' => 'scenes',
                            'action' => 'view',
                            $clip['Scene']['id']
                        ]); ?>
                    </td>
                    <td><?php echo h($clip['Clip']['is_public']? 'Yes' : 'No'); ?>&nbsp;</td>
                    <td><?php echo h($clip['Clip']['created']); ?>&nbsp;</td>
                    <td><?php echo h($clip['Clip']['modified']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('Edit'), [
                            'action' => 'edit',
                            $clip['Clip']['id']
                        ], ['class' => 'btn btn-xs btn-success']); ?>
                        
                        <?php echo $this->Form->postLink(__('Delete'), [
                            'action' => 'delete',
                            $clip['Clip']['id']
                        ], [
                            'class' => 'btn btn-xs btn-success',
                            'confirm' => __('Are you sure you want to delete # %s?', $clip['Clip']['id'])
                        ]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
            echo $this->Paginator->counter([
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ]);
        ?>    </p>
    <div class="paging">
        <?php
            echo $this->element('pagination', ['class' => 'primary']);
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">
        <li><?php echo $this->Html->link(__('New Clip'), ['action' => 'add']); ?></li>
        <li><?php echo $this->Html->link(__('List Scenes'), ['controller' => 'scenes', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Scene'), ['controller' => 'scenes', 'action' => 'add']); ?> </li>
    </ul>
</div>
