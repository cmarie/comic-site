<div class="stories form">
<?php echo $this->BootstrapForm->create('Story', ['type' => 'file']); ?>
	<fieldset>
		<legend><?php echo __('Add Story'); ?></legend>
	<?php
        echo $this->BootstrapForm->input('name');
        echo $this->BootstrapForm->input('file', ['label' => 'Cover Image', 'type' => 'file']);
		echo $this->BootstrapForm->input('is_public');
	?>
	</fieldset>
    <?php echo $this->BootstrapForm->submit(__('Create'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->BootstrapForm->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">

		<li><?php echo $this->Html->link(__('List Stories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Scenes'), array('controller' => 'scenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scene'), array('controller' => 'scenes', 'action' => 'add')); ?> </li>
	</ul>
</div>
