<div class="stories form">
    <?php echo $this->BootstrapForm->create('Story', ['type' => 'file', 'class' => 'form-horizontal']); ?>
    <fieldset>
        <legend><?php echo __('Edit Story'); ?></legend>
        <?php
            echo $this->BootstrapForm->input('id');
            echo $this->BootstrapForm->input('name');
            echo $this->BootstrapForm->input('file', ['label' => 'Cover Image', 'type' => 'file']);
            echo $this->BootstrapForm->input('is_public');
        ?>
    </fieldset>
    <?php echo $this->BootstrapForm->submit(__('Save'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->BootstrapForm->end(); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-pills">

        <li><?php echo $this->Form->postLink(__('Delete'), [
                'action' => 'delete',
                $this->Form->value('Story.id')
            ], ['confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Story.id'))]); ?></li>
        <li><?php echo $this->Html->link(__('List Stories'), ['action' => 'index']); ?></li>
        <li><?php echo $this->Html->link(__('List Scenes'), ['controller' => 'scenes', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Scene'), ['controller' => 'scenes', 'action' => 'add']); ?> </li>
    </ul>
</div>
