<div class="stories index">
	<h2><?php echo __('Stories'); ?></h2>
	<table class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('is_public'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($stories as $story): ?>
	<tr>
		<td><?php echo h($story['Story']['id']); ?>&nbsp;</td>
		<td><?php echo h($story['Story']['name']); ?>&nbsp;</td>
		<td><?php echo h($story['Story']['is_public'] ? 'Yes':'No'); ?>&nbsp;</td>
		<td><?php echo h($story['Story']['created']); ?>&nbsp;</td>
		<td><?php echo h($story['Story']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $story['Story']['id']), ['class' => 'btn btn-xs btn-success']); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $story['Story']['id']), ['class' => 'btn btn-xs btn-success']); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $story['Story']['id']), array('class' => 'btn btn-xs btn-success', 'confirm' => __('Are you sure you want to delete # %s?', $story['Story']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
        echo $this->element('pagination', ['class' => 'primary']);
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('New Story'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Scenes'), array('controller' => 'scenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scene'), array('controller' => 'scenes', 'action' => 'add')); ?> </li>
	</ul>
</div>