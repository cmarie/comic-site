<?php

App::import('Helper', 'Form') ;

class BootstrapFormHelper extends FormHelper
{
    public function create($model = null, $options = array()) {
        $options['class'] = 'form-horizontal';
        $options['inputDefaults'] = array(
            'div' => 'form-group',
            'label' => ['class' => 'col-md-2'],
            'class' => 'form-control',
            'aria-required' => true
        );
        return parent::create($model, $options) ;
    }
}