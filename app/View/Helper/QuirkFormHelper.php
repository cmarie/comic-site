<?php

App::import('Helper', 'Form') ;

class QuirkFormHelper extends FormHelper
{
    private $_model;
    public function create($model = null, $options = [])
    {
        $this->_model = $model;
        $options['inputDefaults'] = array(
            'div' => 'form-group',
            'label' => false,
            'class' => 'form-control',
            'aria-required' => true,
            'error' => array(
                'attributes' => array(
                    'wrap' => 'label', 'class' => 'error mt10'
                )
            )
        );
        return parent::create($model, $options);
    }

    public function input($fieldName, $options = array()){
        if(empty($options['placeholder'])) {
            $options['placeholder'] = Inflector::humanize($this->_model) . ' ' .Inflector::humanize($fieldName);
        }
        return parent::input($fieldName, $options);
    }
    public function checkbox($fieldName, $options = array()){
        $label = $options['label'];
        $desc = !empty($options['desc']) ? $options['desc'] : null;
        $valueOptions = array();
        if (isset($options['default'])) {
            $valueOptions['default'] = $options['default'];
            unset($options['default']);
        }

        $options += array('value' => 1, 'required' => false);
        $options = $this->_initInputField($fieldName, $options) + array('hiddenField' => true);
        $value = current($this->value($valueOptions));
        $output = '';

        if ((!isset($options['checked']) && !empty($value) && $value == $options['value']) ||
            !empty($options['checked'])
        ) {
            $options['checked'] = 'checked';
        }
        if ($options['hiddenField']) {
            $hiddenOptions = array(
                'id' => $options['id'] . '_',
                'name' => $options['name'],
                'value' => ($options['hiddenField'] !== true ? $options['hiddenField'] : '0'),
                'form' => isset($options['form']) ? $options['form'] : null,
                'secure' => false,
            );
            if (isset($options['disabled']) && $options['disabled']) {
                $hiddenOptions['disabled'] = 'disabled';
            }
            $output = $this->hidden($fieldName, $hiddenOptions);
        }
        unset($options['hiddenField']);
        return $this->Html->tag('label', $output . $this->Html->useTag('checkbox', $options['name'], array_diff_key($options, array('name' => null))).' '.$this->Html->tag('span', $label.($desc ? $this->Html->tag('small', $desc, array('class' => 'text-muted')) : null)), array('class' => 'ckbox '.(!empty($options['class']) ? $options['class'] : 'ckbox-default')));

        //return parent::checkbox($fieldName, $options);
    }
    public function select($fieldName, $options = array(), $attributes = array()){
        $attributes['class'] = 'form-control select';
        return parent::select($fieldName, $options, $attributes);
    }
    public function submit($caption = null, $options = [])
    {
        if(empty($options['class'])){
            $options['class'] = 'btn-default';
        }
        $options['class'] = 'btn btn-quirk btn-wide ' . $options['class'];
        return parent::submit($caption, $options);
    }
    public function buttonGroup($buttons = array()){
        $output = null;
        $i = 0;
        foreach($buttons as $name=>$options){
            if(empty($options['class'])){
                $options['class'] = 'btn-success';
            }
            $options['class'] = 'btn btn-quirk btn-wide ' . $options['class'] . ($i == 0 ? ' mr5' : null);
            $output .= $this->Html->tag('button', $name, $options);
            $i++;
        }
        return $output;
    }
}