<?php
App::uses('AppModel', 'Model');
/**
 * Scene Model
 *
 * @property Story $Story
 * @property Clip $Clip
 */
class Scene extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'story_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Clip' => array(
			'className' => 'Clip',
			'foreignKey' => 'scene_id',
			'dependent' => false,
            'conditions' => ['is_public' => true],
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
