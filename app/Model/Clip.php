<?php
App::uses('AppModel', 'Model');
/**
 * Clip Model
 *
 * @property Scene $Scene
 */
class Clip extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Scene' => array(
			'className' => 'Scene',
			'foreignKey' => 'scene_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    public $actsAs = [
        'Image' => [
            'a' => ['w' => 128, 'h' => 128, 'method' => 'crop', 'grayscale' => false, 'validate' => true],
            's' => ['w' => 64, 'h' => 64, 'method' => 'crop', 'grayscale' => false, 'validate' => true],
            'p' => ['w' => 259, 'h' => 259, 'method' => 'crop', 'grayscale' => false, 'validate' => true],
            'f' => ['w' => 1080, 'h' => 1080, 'method' => 'resize', 'grayscale' => false, 'validate' => true]
        ]
    ];
}
