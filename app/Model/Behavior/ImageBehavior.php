<?php

    class ImageBehavior extends ModelBehavior
    {
        private $defaults = ['method' => 'crop', 'quality' => 90];
        private $defaultImages = [];

        public function setup(Model $model, $config = [])
        {
            $this->settings[$model->name]['dir'] = IMAGES . Inflector::tableize($model->name) . DS;
            $this->defaultImages[$model->name] = array_merge($config, ['o' => []]);
        }

        public function beforeValidate(Model $model, $options = [])
        {
            if(!empty($model->data[$model->alias]['file']['tmp_name'])) {
                if($this->__createImageFromTempFile($model->data[$model->alias]['file']['tmp_name'], $model->data[$model->alias]['file']['type'])) {
                    foreach($this->defaultImages[$model->alias] as $img) {
                        if(isset($img['validate']) && $img['validate']) {
                            if(!empty($img['w']) && !empty($img['h'])) {
                                list($w, $h) = getimagesize($model->data[$model->alias]['file']['tmp_name']);
                                if($w < $img['w'] || $h < $img['h']) {
                                    $model->invalidate('file', sprintf('Image size is too small does not meet %sx%s requirement', $img['w'], $img['h']));

                                    return false;
                                }
                            }
                        }
                    }
                } else {
                    $model->invalidate('file', 'Not a valid image file');

                    return false;
                }
            }

            return parent::beforeValidate($model, $options);
        }

        public function afterSave(Model $model, $created, $options = [])
        {
            if(!empty($model->data[$model->alias])) {
                if(!empty($model->data[$model->name]['file']['tmp_name'])) {
                    foreach($this->defaultImages[$model->name] as $filename => $options) {
                        if($filename == 'o') {
                            list($options['w'], $options['h']) = getimagesize($model->data[$model->name]['file']['tmp_name']);
                            $options['method'] = 'resize';
                            $options['quality'] = 100;
                        }
                        $options = $this->__imageSetup($model, $options, $filename);
                        if(@$options['method'] == 'resize' || empty($options['method']) && $this->defaults['method'] == 'resize') {
                            $this->__resize($options);
                        } else if(@$options['method'] == 'crop' || empty($options['method']) && $this->defaults['method'] == 'crop') {
                            $this->__crop($options);
                        }
                    }
                    if(empty($model->data[$model->alias]['has_image'])) {
                        $model->save(['has_image' => true]);
                    }
                } elseif(array_key_exists('has_image', $model->data[$model->alias])) {
                    if(!$model->data[$model->alias]['has_image']) {
                        foreach($this->defaultImages[$model->name] as $filename => $options) {
                            $options = $this->__imageSetup($model, $options, $filename);
                            if(is_file($options['dest'])) {
                                unlink($options['dest']);
                            }
                        }
                        $dir = $this->settings[$model->name]['dir'] . $model->id;
                        if(is_dir($dir)) {
                            rmdir($dir);
                        }
                    }
                }
            }
        }

        public function afterDelete(Model $model)
        {
            foreach($this->defaultImages[$model->name] as $filename => $options) {
                $options = $this->__imageSetup($model, $options, $filename);
                if(is_file($options['dest'])) {
                    unlink($options['dest']);
                }
            }
            $dir = $this->settings[$model->name]['dir'] . $model->id;
            if(is_dir($dir)) {
                rmdir($dir);
            }
        }

        protected function __imageSetup(Model $model, $options, $filename)
        {
            $options['filename'] = sprintf('%s_%s.jpg', sha1(Configure::read('Security.salt') . $model->id), $filename);
            $options['dest'] = $this->settings[$model->name]['dir'] . $model->id . DS . $options['filename'];
            $options['quality'] = (!empty($options['quality']) ? $options['quality'] : $this->defaults['quality']);
            if(!is_dir($this->settings[$model->name]['dir'] . $model->id)) {
                mkdir($this->settings[$model->name]['dir'] . $model->id, 0700, true);
            }
            if(!empty($model->data[$model->name]['file'])) {
                $options['format'] = $model->data[$model->name]['file']['type'];

                return array_merge($options, $model->data[$model->name]['file']);
            } else {
                return $options;
            }
        }

        protected function __createImageFromTempFile($temp_name, $format)
        {
            switch($format) {
                case 'image/gif':
                    return @imagecreatefromgif($temp_name);
                    break;
                case 'image/png':
                    return @imagecreatefrompng($temp_name);
                    break;
                case 'image/x-png':
                    return @imagecreatefrompng($temp_name);
                    break;
                case 'image/jpeg':
                    return @imagecreatefromjpeg($temp_name);
                    break;
                case 'image/pjpeg':
                    return @imagecreatefromjpeg($temp_name);
                    break;
                case 'application/octet-stream':
                    return @imagecreatefromjpeg($temp_name);
                    break;
                default:
                    return false;
                    break;
            }
        }

        protected function __resize($options)
        {
            $img = $this->__createImageFromTempFile($options['tmp_name'], $options['format']);
            if($img) {
                if(!empty($options['watermark'])) {
                    $watermark = imagecreatefrompng($options['watermark']);
                    list($watermark_width, $watermark_height) = getimagesize($options['watermark']);
                }
                list($org_width, $org_height) = getimagesize($options['tmp_name']);
                $options['w'] = ($options['w'] > $org_width ? $org_width : $options['w']);
                $options['h'] = ($options['h'] > $org_height ? $org_height : $options['h']);
                $xoffset = 0;
                $yoffset = 0;
                $ratio_orig = $org_width / $org_height;
                if(($options['w'] / $options['h']) > $ratio_orig) {
                    $options['w'] = $options['h'] * $ratio_orig;
                } else {
                    $options['h'] = $options['w'] / $ratio_orig;
                }
                $img_n = imagecreatetruecolor($options['w'], $options['h']);
                imagecolortransparent($img, imagecolorallocate($img, 255, 255, 255));
                imagecopyresampled($img_n, $img, 0, 0, $xoffset, $yoffset, $options['w'], $options['h'], $org_width, $org_height);
                if(!empty($options['watermark'])) {
                    imagealphablending($watermark, true);
                    imagecopy($img_n, $watermark, $options['w'] - $watermark_width - 10, $options['h'] - $watermark_height - 10, 0, 0, $watermark_width, $watermark_height);
                }
                if(!empty($options['grayscale'])) {
                    imagefilter($img_n, IMG_FILTER_GRAYSCALE);
                }

                return imagejpeg($img_n, $options['dest'], $options['quality']);
            }
        }

        protected function __crop($options)
        {
            $img = $this->__createImageFromTempFile($options['tmp_name'], $options['format']);
            if($img) {
                if(!empty($options['watermark'])) {
                    $watermark = imagecreatefrompng($options['watermark']);
                    list($watermark_width, $watermark_height) = getimagesize($options['watermark']);
                }
                list($org_width, $org_height) = getimagesize($options['tmp_name']);
                $xoffset = 0;
                $yoffset = 0;
                $options['h'] = ($options['h'] > $org_height ? $org_height : $options['h']);
                $options['w'] = ($options['w'] > $org_width ? $org_width : $options['w']);
                if(($org_width / $options['w']) > ($org_height / $options['h'])) {
                    $resize_percentage = $options['h'] / $org_height;
                    $resized_width = $org_width * $resize_percentage;
                    $crop_percentage = $options['w'] / $resized_width;
                    $xoffset = ($org_width - ($org_width * $crop_percentage)) / 2;
                    $org_width = $org_width * $crop_percentage;
                } else {
                    $resize_percentage = $options['w'] / $org_width;
                    $resized_height = $org_height * $resize_percentage;
                    $crop_percentage = $options['h'] / $resized_height;
                    $yoffset = ($org_height - ($org_height * $crop_percentage)) / 2;
                    $org_height = $org_height * $crop_percentage;
                }
                $img_n = imagecreatetruecolor($options['w'], $options['h']);
                imagecolortransparent($img, imagecolorallocate($img, 255, 255, 255));
                imagecopyresampled($img_n, $img, 0, 0, $xoffset, $yoffset, $options['w'], $options['h'], $org_width, $org_height);
                if(!empty($options['watermark'])) {
                    imagealphablending($watermark, true);
                    imagecopy($img_n, $watermark, $options['w'] - $watermark_width - 10, $options['h'] - $watermark_height - 10, 0, 0, $watermark_width, $watermark_height);
                }
                if(!empty($options['grayscale'])) {
                    imagefilter($img_n, IMG_FILTER_GRAYSCALE);
                }

                return imagejpeg($img_n, $options['dest'], $options['quality']);
            }
        }
    }
