<?php
App::uses('Clip', 'Model');

/**
 * Clip Test Case
 */
class ClipTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.clip',
		'app.scene',
		'app.story'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Clip = ClassRegistry::init('Clip');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Clip);

		parent::tearDown();
	}

}
